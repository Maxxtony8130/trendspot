import { View, Text, Image, TouchableOpacity } from 'react-native'
import React from 'react';
import Animated, { interpolate, useAnimatedStyle } from 'react-native-reanimated';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useDrawerProgress } from '@react-navigation/drawer';
import { COLORS, icons } from '../constants';
import { Home, Notification, Wallet, Favourite, Orders, Coupons, Settings } from '../screen';
import { Invite } from '../screen';
import { Help } from '../screen';


const Stack = createNativeStackNavigator();

const Screens = ({ navigation }) => {

    const progress = useDrawerProgress();

    const viewStyle = useAnimatedStyle(() => {
        const scale = interpolate(
            progress.value,
            [0, 1],
            [1, .85]
        )
        const borderRadius = interpolate(
            progress.value,
            [0, 1],
            [0, 20],
        )
        return {
            transform: [{ scale }],
            borderRadius,
        }
    })

    return (
        <Animated.View style={[{ flex: 1 }, viewStyle]}>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name="Home" component={Home}  />
                <Stack.Screen name="My Wallet" component={Wallet} />
                <Stack.Screen name="Notifications" component={Notification} />
                <Stack.Screen name="My Favroites" component={Favourite} />
                <Stack.Screen name="Orders" component={Orders} />
                <Stack.Screen name="Coupons" component={Coupons} />
                <Stack.Screen name="Settings" component={Settings} />
                <Stack.Screen name="Invite a Friend" component={Invite} />
                <Stack.Screen name="Help Center" component={Help} />

            </Stack.Navigator>
        </Animated.View >
    )
}

export default Screens