import {View, Text} from 'react-native';
import React, {useEffect} from 'react';
import Lottie from 'lottie-react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Splash = ({navigation}: {navigation: any}) => {

  const isLogin = async () => {
    const jsonValue: any = await AsyncStorage.getItem('DATA');
    const data = JSON.parse(jsonValue);
    if (data !== null) {
      navigation.navigate('Main');
    } else {
      navigation.navigate('Login');
    }
  };

  useEffect(() => {
    setTimeout(() => {
      isLogin();
    }, 3500);
  });

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Lottie
        source={require('../../assets/images/AK.json')}
        autoPlay
        loop
        style={{height: 200, width: '100%'}}
      />
    </View>
  );
};

export default Splash;
