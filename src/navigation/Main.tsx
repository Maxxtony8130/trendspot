import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Pressable,
  Alert,
} from 'react-native';
import React, {useEffect, useMemo, useState} from 'react';
import Screens from './Screens';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
} from '@react-navigation/drawer';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {COLORS, FONTS, SIZES, icons} from '../constants';
import {Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import DialogBox from '../constants/Alert';

const Drawer = createDrawerNavigator();

interface Props {
  icon: any;
  label: any;
}

const menu = [
  {icon: icons.home, label: 'Home'},
  {icon: icons.wallet, label: 'My Wallet'},
  {icon: icons.notification, label: 'Notifications'},
  {icon: icons.favourite, label: 'My Favroites'},
  {icon: icons.location, label: 'Orders'},
  {icon: icons.coupon, label: 'Coupons'},
  {icon: icons.setting, label: 'Settings'},
  {icon: icons.profile, label: 'Invite a Friend'},
  {icon: icons.help, label: 'Help Center'},
];

const DrawerItems = ({icon, label}: Props) => (
  <View
    style={{
      flexDirection: 'row',
      height: 40,
      marginBottom: SIZES.base,
      alignItems: 'center',
      paddingLeft: SIZES.radius,
      borderRadius: SIZES.base,
      backgroundColor: 'transparent',
    }}>
    <Image
      source={icon}
      style={{
        width: 20,
        height: 20,
        tintColor: COLORS.white,
      }}
    />
    <Text
      style={{
        marginLeft: SIZES.radius,
        color: COLORS.white,
        ...FONTS.h3,
      }}>
      {label}
    </Text>
  </View>
);

const CustomDrawerContent = (props: any) => {
  const [selected, setSelected] = useState(0);
  const [user, setUser] = useState<any>([]);

  const userdata = async () => {
    const jsonValue: any = await AsyncStorage.getItem('DATA');
    const data = JSON.parse(jsonValue);
    setUser(data);
  };
  useEffect(() => {
    userdata();
  }, []);


  const handleLogout = async () => {
    try {
      await AsyncStorage.clear();
      console.log('logout completed');
      props.navigation.navigate('Login');
    } catch (e: any) {
      console.log(e.message);
    }
  };

  return (
    <DrawerContentScrollView
      {...props}
      scrollEnabled={false}
      contentContainerStyle={{flex: 1}}>
      <View style={{padding: SIZES.h4, flex: 1}}>
        {/* CLOSE */}
        <TouchableOpacity onPress={() => props.navigation.closeDrawer()}>
          <Image
            source={icons.cross}
            style={{height: 30, width: 30, tintColor: COLORS.white}}
          />
        </TouchableOpacity>
        {/* profile */}
        <TouchableOpacity
          style={{
            marginVertical: SIZES.radius,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Image
            source={require('../assets/images/profile.png')}
            style={{height: 50, width: 50, borderRadius: SIZES.radius}}
          />
          <View style={{marginLeft: SIZES.radius}}>
            <Text style={{color: COLORS.white, ...FONTS.h3}}>{user.name}</Text>
            <Text style={{color: COLORS.white, ...FONTS.body4}}>
              {user.email}
            </Text>
          </View>
        </TouchableOpacity>
        {/* menu */}
        <View style={{flex: 1}}>
          {menu.map((item: any, index: any) => {
            return (
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  height: 40,
                  marginBottom: SIZES.base,
                  alignItems: 'center',
                  paddingLeft: SIZES.radius,
                  borderRadius: SIZES.base,
                  backgroundColor:
                    selected === index
                      ? COLORS.transparentBlack1
                      : 'transparent',
                }}
                key={index}
                onPress={() => {
                  setSelected(index), props.navigation.navigate(item.label);
                }}>
                <Image
                  source={item.icon}
                  style={{
                    width: 20,
                    height: 20,
                    tintColor: selected === index ? COLORS.white : COLORS.white,
                  }}
                />
                <Text
                  style={{
                    marginLeft: SIZES.radius,
                    color: selected === index ? COLORS.white : COLORS.white,
                    ...FONTS.h3,
                  }}>
                  {item.label}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>
        <TouchableOpacity onPress={() => handleLogout()}>
          <DrawerItems label="Logout" icon={icons.logout} />
        </TouchableOpacity>
      </View>
    </DrawerContentScrollView>
  );
};

export default () => {
  return (
    <LinearGradient colors={['#000428', '#004e92']} style={{flex: 1}}>
      <Drawer.Navigator
        screenOptions={{
          headerShown: false,
          swipeEnabled: false,
          drawerType: 'slide',
          overlayColor: 'transparent',
          drawerStyle: {
            width: '60%',
            flex: 1,
            backgroundColor: 'transparent',
          },
          sceneContainerStyle: {
            backgroundColor: 'transparent',
          },
        }}
        drawerContent={props => {
          return <CustomDrawerContent {...props} />;
        }}>
        <Drawer.Screen name="Screens">
          {props => <Screens {...props} />}
        </Drawer.Screen>
      </Drawer.Navigator>
    </LinearGradient>
  );
};
