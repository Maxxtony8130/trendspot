import {View, Text, Alert} from 'react-native';
import React, {useState} from 'react';
import {COLORS, FONTS, SIZES} from '../../constants';
import Heading from '../../constants/Heading';
import Input from '../../constants/Input';
import Button from '../../constants/Button';
import LinearGradient from 'react-native-linear-gradient';
import SocialButton from '../../constants/SocialButton';
import Loader from '../../constants/Loader';
import firestore from '@react-native-firebase/firestore';
import AsyncStorage from '@react-native-async-storage/async-storage';

interface Props {
  navigation: any;
}

const Login = ({navigation}: Props) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [modal, setModal] = useState(false);

  const [showError, setShowError] = useState(false);
  const [errors, setError] = useState<any>({});

  const getErrors = (email: any, password: any) => {
    const errors: any = {};

    if (!email) {
      errors.email = 'Please Enter your email';
    } else if (!email.includes('@') || !email.includes('.com')) {
      errors.email = 'Please Enter a valid email';
    }
    if (!password) {
      errors.password = 'Please Enter your password';
    }
    return errors;
  };

  const getUser = async () => {
    setModal(true);
    firestore()
      .collection('users')
      // Filter results
      .where('email', '==', email)
      .get()
      .then((q: any) => {
        setModal(false);
        if (q.docs[0]._data !== null) {
          if (
            q.docs[0]._data.email === email &&
            q.docs[0]._data.password === password
          ) {
            const jsonValue = JSON.stringify(q.docs[0]._data);
            goToNextScreen(jsonValue);
          }
        }
      })
      .catch(error => {
        setModal(false);
        Alert.alert('Please check your credentials');
      });
  };

  const goToNextScreen = async (jsonValue: any) => {
    await AsyncStorage.setItem('DATA', jsonValue);
    navigation.navigate('Main');
  };

  return (
    <View style={{flex: 1}}>
      <LinearGradient colors={['#000428', '#004e92']} style={{flex: 1}}>
        <View
          style={{
            padding: 30,
            backgroundColor: COLORS.white,
            borderTopRightRadius: 150,
            position: 'absolute',
            bottom: 0,
            width: '100%',
          }}>
          {/* Heading */}
          <Heading text="Welcome Back...." />
          {/* Input Field */}
          <Input
            value={email}
            name="email"
            onChangeText={(text: any) => setEmail(text)}
            placeholder="Enter your email address"
            keyboardType="email-address"
            maxLength={100}
          />
          <Input
            value={password}
            name="lock"
            onChangeText={(text: any) => setPassword(text)}
            placeholder="Password"
            isPassword={password.length !== 0 ? true : false}
          />

          {/* Custom BuTTON */}
          <Button label="Sign In" onPress={() => getUser()} />
          {/* Already have account line */}
          <Text
            style={{
              textAlign: 'right',
              ...FONTS.h4,
              fontWeight: 'bold',
              marginRight: SIZES.radius,
              color: COLORS.black,
            }}>
            New User ?{' '}
            <Text
              onPress={() => navigation.navigate('Register')}
              style={{color: '#004e92'}}>
              Sign Up
            </Text>
          </Text>
          {/* Seprator */}
          <View
            style={{
              marginVertical: SIZES.radius,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                width: 90,
                height: 1.5,
                backgroundColor: 'black',
                marginTop: 5,
              }}
            />
            <Text
              style={{
                color: 'black',
                ...FONTS.h4,
                fontWeight: 'bold',
                marginHorizontal: 5,
              }}>
              {' '}
              Sign In with{' '}
            </Text>
            <View
              style={{
                width: 90,
                height: 1.5,
                backgroundColor: 'black',
                marginTop: 5,
              }}
            />
          </View>
          <View
            style={{
              marginVertical: SIZES.body2,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-evenly',
            }}>
            <SocialButton source={require('../../assets/images/google.png')} />
            <SocialButton
              source={require('../../assets/images/facebook.png')}
            />
            <SocialButton source={require('../../assets/images/apple.png')} />
          </View>
        </View>
        <Loader modalVisible={modal} setModalVisible={setModal} />
      </LinearGradient>
    </View>
  );
};

export default Login;
