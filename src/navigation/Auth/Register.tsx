import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  Alert,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {COLORS, FONTS, SIZES} from '../../constants';
import Heading from '../../constants/Heading';
import Input from '../../constants/Input';
import Button from '../../constants/Button';
import LinearGradient from 'react-native-linear-gradient';
import Loader from '../../constants/Loader';
import uuid from 'react-native-uuid';
import firestore from '@react-native-firebase/firestore';

interface Props {
  navigation: any;
}

const Register = ({navigation}: Props) => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [number, setNumber] = useState('');
  const [modal, setModal] = useState(false);

  const [showError, setShowError] = useState(false);
  const [errors, setError] = useState<any>({});

  const [isExist, setIsExist] = useState<any>([]);

  useEffect(() => {
    firestore()
      .collection('users')
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(data => {
          const info = data.data();
          // console.log('hello baba', info);
          setIsExist(info);
        });
      });
  },[]);


  const getErrors = (
    name: any,
    email: any,
    password: any,
    confirmPassword: any,
    number: any,
  ) => {
    const errors: any = {};
    if (!name) {
      errors.name = 'Please Enter your name';
    }
    if (!email) {
      errors.email = 'Please Enter your email';
    } else if (!email.includes('@') || !email.includes('.com')) {
      errors.email = 'Please Enter a valid email';
    } else if (isExist.email === email) {
      errors.email = 'Email already exists for other user';
    }
    if (!password) {
      errors.password = 'Please Enter your password';
    } else if (password.length < 1) {
      errors.password = 'Password must be long at least 8 characters';
    }
    if (!confirmPassword) {
      errors.confirmPassword = 'Please Enter your Password';
    } else if (password !== confirmPassword) {
      errors.confirmPassword = 'Password do not match';
    }
    if (!number) {
      errors.number = 'Please Enter your Mobile Number';
    } else if (number.length < 10) {
      errors.number = 'Number length should more than 9 digits';
    } else if (isExist.number === number) {
      errors.number = 'Number already registered for another user';
    }

    return errors;
  };

  const setUser = () => {
    const errors: any = getErrors(
      name,
      email,
      password,
      confirmPassword,
      number,
    );
    if (Object.keys(errors).length > 0) {
      setShowError(true);
      setError(showError && errors);
    } else {
      setModal(true);
      const userId: any = uuid.v4();
      firestore()
        .collection('users')
        .doc(userId)
        .set({
          name: name,
          email: email,
          password: password,
          confirmPassword: confirmPassword,
          number: number,
          userId: userId,
        })
        .then(res => {
          setShowError(false);
          setModal(false);
          navigation.navigate('Login');
          setEmail('');
          setConfirmPassword('');
          setPassword('');
          setName('');
          setNumber('');
        });
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={{flex: 1}}>
      <LinearGradient colors={['#000428', '#004e92']} style={{flex: 1}}>
        <View
          style={{
            padding: 30,
            backgroundColor: COLORS.white,
            borderTopRightRadius: 150,
            position: 'absolute',
            bottom: 0,
            width: '100%',
          }}>
          {/* Heading */}
          <Heading text="Get Started...." />
          {/* Input Field */}
          <Input
            value={name}
            name="account"
            onChangeText={(text: any) => setName(text)}
            placeholder="Enter your name"
          />
          {errors.name && (
            <Text
              style={{
                color: COLORS.red,
                ...FONTS.body5,
                marginLeft: SIZES.radius,
              }}>
              {errors.name}
            </Text>
          )}
          <Input
            value={email}
            name="email"
            onChangeText={(text: any) => setEmail(text)}
            placeholder="Enter your email address"
            keyboardType="email-address"
            maxLength={100}
          />
          {errors.email && (
            <Text
              style={{
                color: COLORS.red,
                ...FONTS.body5,
                marginLeft: SIZES.radius,
              }}>
              {errors.email}
            </Text>
          )}
          <Input
            value={password}
            name="lock"
            onChangeText={(text: any) => setPassword(text)}
            placeholder="Password"
            isPassword={password.length !== 0 ? true : false}
          />
          {errors.password && (
            <Text
              style={{
                color: COLORS.red,
                ...FONTS.body5,
                marginLeft: SIZES.radius,
              }}>
              {errors.password}
            </Text>
          )}
          <Input
            value={confirmPassword}
            name="lock"
            onChangeText={(text: any) => setConfirmPassword(text)}
            placeholder="Confirm Password"
            isPassword={confirmPassword.length !== 0 ? true : false}
          />
          {errors.confirmPassword && (
            <Text
              style={{
                color: COLORS.red,
                ...FONTS.body5,
                marginLeft: SIZES.radius,
              }}>
              {errors.confirmPassword}
            </Text>
          )}
          <Input
            value={number}
            name="phone"
            onChangeText={(text: any) => setNumber(text)}
            secureTextEntry={false}
            maxLength={10}
            placeholder="Enter your number..."
            keyboardType="number-pad"
          />
          {errors.number && (
            <Text
              style={{
                color: COLORS.red,
                ...FONTS.body5,
                marginLeft: SIZES.radius,
              }}>
              {errors.number}
            </Text>
          )}
          {/* Custom BuTTON */}
          <Button label="Sign Up" onPress={() => setUser()} />
          {/* Already have account line */}
          <Text
            style={{
              textAlign: 'right',
              ...FONTS.h4,
              fontWeight: 'bold',
              marginRight: SIZES.radius,
              color: COLORS.black,
            }}>
            Already Have an Accoun ?{' '}
            <Text
              onPress={() => navigation.navigate('Login')}
              style={{color: '#004e92'}}>
              Sign In
            </Text>
          </Text>
        </View>
        <Loader modalVisible={modal} setModalVisible={setModal} />
      </LinearGradient>
    </KeyboardAvoidingView>
  );
};

export default Register;
