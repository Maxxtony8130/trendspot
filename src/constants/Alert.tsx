import {View, Text} from 'react-native';
import React from 'react';
import Modal from 'react-native-modal';

const DialogBox = () => {
  return (
    <View>
      <Modal>
        <View style={{flex: 1}}>
          <Text>I am the modal content!</Text>
        </View>
      </Modal>
    </View>
  );
};

export default DialogBox;
