import { View, Text, TextInput, Pressable } from 'react-native'
import React, { useState } from 'react'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Eye from 'react-native-vector-icons/MaterialCommunityIcons';

const Input = (props) => {
  const [hide, setHide] = useState(true);
  return (
    <View
      style={{
        marginTop: 15,
        flexDirection: 'row',
        paddingHorizontal: 20,
        alignItems: 'center',
        width: '100%',
        borderWidth: 1,
        borderColor: '#004e92',
        borderRadius: 50,
      }}>
      <Icon name={props.name} size={20}  color='#004e92'/>
      <TextInput
        placeholderTextColor={'black'}
        style={{ padding: 8, width: '85%', color: 'black' }}
        secureTextEntry={props.isPassword && hide}
        {...props}

      />
      {props.isPassword && (
        <Pressable onPress={() => setHide(!hide)}>
          <Eye
            name={hide ? 'eye-off' : 'eye'}
            size={20}
            color={hide ? '#004e92' : '#004e92'}
          />
        </Pressable>
      )}
    </View>
  )
}

export default Input