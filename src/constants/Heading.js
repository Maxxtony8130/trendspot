import { View, Text } from 'react-native'
import React from 'react'
import { FONTS } from './theme'

const Heading = (props) => {
    return (
        <View style={{ marginBottom: 15 }}>
            <Text
                style={{
                    ...FONTS.h1,
                    fontWeight: 'bold',
                    fontStyle: "italic",
                    color: '#004e92',
                }}
                {...props}
            >
                {props.text}
            </Text>
        </View>
    )
}

export default Heading