import { View, Text, Image, TouchableOpacity } from 'react-native'
import React from 'react'

const SocialButton = (props) => {
    return (
        <TouchableOpacity onPress={props.onPress} >
            <Image source={props.source} resizeMode='cover' style={{ height: 35, width: 35 }} {...props} />
        </TouchableOpacity>
    )
}

export default SocialButton