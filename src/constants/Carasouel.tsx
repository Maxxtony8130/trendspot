import {Image, View, Dimensions} from 'react-native';
import React from 'react';
import Carousel from 'react-native-reanimated-carousel';
import {GestureHandlerRootView} from 'react-native-gesture-handler';

const Carasouel = () => {
  const width = Dimensions.get('window').width;

  const data = [
    {
      id: 1,
      image: 'https://sslimages.shoppersstop.com/sys-master/root/hfd/h25/29875115982878/Top-Banner-jack-%26-jones-Web6-2-2023kgnhui54_4868497.jpg',
      title: 'image 1',
    },
    {
      id: 2,
      image: 'https://sslimages.shoppersstop.com/sys-master/root/h66/h74/29875116179486/Top-Banner-pepe-Webdhwgfduwwu.jpg',
      title: 'image 2',
    },
    {
      id: 3,
      image: 'https://sslimages.shoppersstop.com/sys-master/root/h66/h74/29875116179486/Top-Banner-pepe-Webdhwgfduwwu.jpg',
      title: 'image 3',
    },
    {
      id: 4,
      image: 'https://sslimages.shoppersstop.com/sys-master/root/h66/h74/29875116179486/Top-Banner-pepe-Webdhwgfduwwu.jpg',
      title: 'image 4',
    },
    {
      id: 5,
      image: 'https://sslimages.shoppersstop.com/sys-master/root/h66/h74/29875116179486/Top-Banner-pepe-Webdhwgfduwwu.jpg',
      title: 'image 5',
    },
  ]

  const Item = ({item}: {item: any}) => (
    <View>
      <Image
        style={{width: '100%', height: '100%'}}
        source={{uri: item.image}}
        resizeMode='cover'
      />
    </View>
  );

  return (
    <View style={{paddingHorizontal: 13}}>
      <GestureHandlerRootView>
        <Carousel
          loop
          width={width * .9}
          height={width / 2}
          mode="parallax"
          modeConfig={{
            parallaxScrollingScale: 0.9,
            parallaxScrollingOffset: 50,
          }}
          autoPlay={true}
          data={data}
          scrollAnimationDuration={1000}
          renderItem={({item}) => <Item item={item} />}
        />
      </GestureHandlerRootView>
    </View>
  );
};

export default Carasouel;
