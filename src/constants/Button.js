import { View, Text, Pressable } from 'react-native'
import React from 'react';
import LinearGradient from 'react-native-linear-gradient';

const Button = (props) => {
    return (
        <LinearGradient
            colors={['#000428', '#004e92']}
            style={{
                padding: 8,
                borderTopLeftRadius: 50,
                borderBottomRightRadius: 50,
                marginVertical: 15,
            }}
            {...props}
        >
            <Pressable onPress={props.onPress}>
                <Text
                    style={{
                        textAlign: 'center',
                        fontWeight: 'bold',
                        fontStyle: 'italic',
                        fontSize: 20,
                        color: 'white',
                    }}>
                    {props.label}
                </Text>
            </Pressable>
        </LinearGradient>
    )
}

export default Button