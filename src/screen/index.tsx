// Home
import Home from './Home/Home';

// Wallet
import Wallet from './Wallet/Wallet';

// Notification
import Notification from './Notification/Notification';

//My Favorite
import Favourite from './Favourite/Favourite';

//Order
import Orders from './Orders/Orders';

//Coupons
import Coupons from './Coupons/Coupons';

//Settings
import Settings from './Settings/Settings';

//Invite a Friend
import Invite from './Invite/Invite';

//help Center
import Help from './Help/Help';

export {
  Home,
  Wallet,
  Notification,
  Favourite,
  Orders,
  Coupons,
  Settings,
  Invite,
  Help,
};
