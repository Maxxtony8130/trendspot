import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  ScrollView,
  FlatList,
  Dimensions,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {Image} from 'react-native';
import {COLORS, FONTS, SIZES, icons} from '../../constants';
import Icon from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';
import Carasouel from '../../constants/Carasouel';
import axios from 'axios';

interface Props {
  navigation: any;
}

const width = Dimensions.get('window').width;

const Home = ({navigation}: Props) => {
  const [category, setCategory] = useState<any>([]);
  const [product, setProduct] = useState<any>([]);
  const [selected, setSelected] = useState(0);

  const Category = async () => {
    const options = {
      method: 'GET',
      url: 'https://apidojo-hm-hennes-mauritz-v1.p.rapidapi.com/categories/list',
      params: {
        lang: 'en',
        country: 'in',
      },
      headers: {
        'X-RapidAPI-Key': '3730073ddbmshfc297670c8f3585p12403cjsn341f63a15e3a',
        'X-RapidAPI-Host': 'apidojo-hm-hennes-mauritz-v1.p.rapidapi.com',
      },
    };

    try {
      const response = await axios.request(options);
      setCategory(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  const Product = async () => {
    const options = {
      method: 'GET',
      url: 'https://apidojo-hm-hennes-mauritz-v1.p.rapidapi.com/products/list',
      params: {
        country: 'in',
        lang: 'en',
        currentpage: '0',
        pagesize: '30',
        categories: 'men_all',
        concepts: 'H&M MAN',
      },
      headers: {
        'X-RapidAPI-Key': '3730073ddbmshfc297670c8f3585p12403cjsn341f63a15e3a',
        'X-RapidAPI-Host': 'apidojo-hm-hennes-mauritz-v1.p.rapidapi.com',
      },
    };

    try {
      const response = await axios.request(options);
      setProduct(response.data.results);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    Category();
    Product();
  }, []);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
      }}>
      <LinearGradient colors={['#000428', '#004e92']} style={{flex: 1}}>
        <View style={{padding: 20}}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
              <Image
                source={icons.menu}
                style={{height: 20, width: 20, tintColor: COLORS.white}}
              />
            </TouchableOpacity>
            <Text
              style={{color: COLORS.white, ...FONTS.h2, fontWeight: 'bold'}}>
              Home
            </Text>
            <TouchableOpacity>
              <Icon name="ios-cart" size={25} color={COLORS.white} />
            </TouchableOpacity>
          </View>
          <View
            style={{
              paddingVertical: 20,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View
              style={{
                paddingHorizontal: 8,
                backgroundColor: COLORS.transparentBlack1,
                width: '100%',
                borderRadius: 10,
                position: 'relative',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Icon name="ios-search" size={20} color={COLORS.white2} />
              <TextInput
                placeholder="Search For Your Items....."
                placeholderTextColor={COLORS.white2}
                style={{
                  width: '90%',
                  height: 50,
                  color: COLORS.white2,
                  marginLeft: SIZES.radius,
                }}
                onFocus={() => navigation.navigate('SearchProduct')}
              />
            </View>
          </View>
        </View>

        <View
          style={{
            paddingTop: 35,
            paddingHorizontal: 5,
            backgroundColor: COLORS.white,
            flex: 1,
            borderTopLeftRadius: 80,
            borderTopRightRadius: 80,
          }}>
          <ScrollView>
            <Carasouel />
            {/* Categories */}
            <View style={{paddingVertical: 16}}>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={category}
                nestedScrollEnabled
                renderItem={({item, index}) => {
                  console.log(item);
                  return (
                    <TouchableOpacity
                      style={{
                        padding: 5,
                        backgroundColor:
                          selected === index ? COLORS.black : COLORS.white,
                        marginLeft: SIZES.radius,
                        borderWidth: 1,
                        borderRadius: SIZES.padding,
                      }}
                      key={index}
                      onPress={() => setSelected(index)}>
                      <Text
                        style={{
                          ...FONTS.h4,
                          fontWeight: '700',
                          color:
                            selected === index ? COLORS.white : COLORS.black,
                        }}>
                        {item.CatName}
                      </Text>
                    </TouchableOpacity>
                  );
                }}
                keyExtractor={item => item.id}
              />
            </View>
            <View style={{padding: 10}}>
              <FlatList
                data={product}
                nestedScrollEnabled
                keyExtractor={item => item.id}
                renderItem={({item, index}) => {
                  return (
                    <View
                      style={{
                        margin: 10,
                        padding: 10,
                        backgroundColor: COLORS.white2,
                        elevation: 35,
                        shadowColor: COLORS.eblue,
                        borderRadius: 10,
                        marginBottom: SIZES.radius,
                      }}>
                      <Image
                        resizeMode="contain"
                        style={{height: 200, width: '100%'}}
                        source={{uri: item.images[0].baseUrl}}
                      />
                      <Text>{item.name}</Text>
                    </View>
                  );
                }}
              />
            </View>
          </ScrollView>
        </View>
      </LinearGradient>
    </View>
  );
};

export default Home;
