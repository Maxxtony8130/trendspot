import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {Image} from 'react-native';
import {COLORS, icons} from '../../constants';

interface Props {
  navigation: any;
}

const Coupons = ({navigation}: Props) => {
  return (
    <View style={{flex: 1, backgroundColor: 'blue', padding: 20}}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <TouchableOpacity onPress={() => navigation.openDrawer()}>
          <Image
            source={icons.menu}
            style={{height: 20, width: 20, tintColor: COLORS.black}}
          />
        </TouchableOpacity>
        <Text>Coupons</Text>
        <View />
      </View>
    </View>
  );
};

export default Coupons;
