import React, {useEffect, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Register from './src/navigation/Auth/Register';
import Login from './src/navigation/Auth/Login';
import Main from './src/navigation/Main';
import Splash from './src/navigation/Onboarding/Splash';
import SearchProduct from './src/screen/SearchProduct/SearchProduct';

const Stack = createNativeStackNavigator();

const App = () => {


  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
          contentStyle: {
            backgroundColor: 'white',
          },
        }}>
        <Stack.Screen name="Splash" component={Splash} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Main" component={Main} />
        <Stack.Screen name="SearchProduct" component={SearchProduct} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
